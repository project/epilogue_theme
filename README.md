CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Epilogue is a responsive theme with clean lines and a minimalistic look
and feel.


REQUIREMENTS
------------

This theme require the following theme:

 * Bootstrap (https://www.drupal.org/project/bootstrap)


INSTALLATION
------------

As this theme is based on bootstrap you should download and enable
Drupal Bootstrap project.

Steps for smooth installation of epilogue theme:
  - Theme file can be downloaded from the link
    https://www.drupal.org/project/epilogue_theme - Extract the downloaded
    file to the themes directory.

  - Goto Admin > Appearance, find epilogue theme and choose 'Install and
    set as default' option.

  - You have now enabled your theme.


CONFIGURATION
-------------

The theme sections can be customized from the theme settings in
admin area.


MAINTAINERS
-----------

Current maintainer:
 * zyxware - https://www.drupal.org/u/zyxware
